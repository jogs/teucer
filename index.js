'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var XMLHttpRequest = window.XMLHttpRequest;

module.exports = function () {
  function Teucer(url) {
    _classCallCheck(this, Teucer);

    this.url = url;
    return this;
  }

  _createClass(Teucer, [{
    key: 'get',
    value: function get() {
      var payload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      return this.method('GET', this.url, payload, headers);
    }
  }, {
    key: 'post',
    value: function post() {
      var payload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      return this.method('POST', this.url, payload, headers);
    }
  }, {
    key: 'put',
    value: function put() {
      var payload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      return this.method('PUT', this.url, payload, headers);
    }
  }, {
    key: 'delete',
    value: function _delete() {
      var payload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      return this.method('DELETE', this.url, payload, headers);
    }
  }, {
    key: 'request',
    value: function request() {
      var returnEvent = function returnEvent(evt) {
        return evt;
      };
      var progressCalc = function progressCalc(evt) {
        evt.lengthComputable ? evt.loaded / evt.total : 0;
      };
      var ajax = new XMLHttpRequest();
      ajax.setHeaders = function (headers) {
        for (var header in headers) {
          if (headers.hasOwnProperty(header)) {
            ajax.setRequestHeader(header, headers[header]);
          }
        }
      };
      ajax.onprogress = progressCalc;
      ajax.onload = returnEvent;
      ajax.onerror = returnEvent;
      ajax.onabort = returnEvent;
      ajax.onloadend = returnEvent;
      ajax.onloadstart = returnEvent;
      ajax.upload.onprogress = progressCalc;
      ajax.upload.onload = returnEvent;
      ajax.upload.onerror = returnEvent;
      ajax.upload.onabort = returnEvent;
      ajax.upload.onloadend = returnEvent;
      ajax.upload.onloadstart = returnEvent;
      ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) return ajax.responseText;
      };
      return ajax;
    }
  }, {
    key: 'method',
    value: function method(_method, url, payload) {
      var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

      var ajax = this.request();
      return new Promise(function (resolve, reject) {
        var uri = url;
        if (_method === 'GET' || _method === 'DELETE') {
          uri += '?';
          var argcount = 0;
          for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
              if (argcount++) {
                uri += '&';
              }
              uri += encodeURIComponent(key) + '=' + encodeURIComponent(payload[key]);
            }
          }
          ajax.open(_method, uri, true);
          ajax.setHeaders(headers);
          ajax.send();
        } else {
          ajax.open(_method, uri, true);
          ajax.setHeaders(headers);
          if (payload.constructor === window.FormData.constructor) {
            ajax.send(payload);
          } else {
            ajax.setRequestHeader('Content-Type', 'application/json');
            ajax.send(JSON.stringify(payload));
          }
        }
        ajax.onload = function () {
          if (this.status >= 200 && this.status < 300) {
            resolve(JSON.parse(this.response));
          } else {
            if (/^\{|\[/.test(this.response)) {
              reject(JSON.parse(this.response));
            } else {
              reject(this.response);
            }
          }
        };
        ajax.onerror = function () {
          reject(this.statusText);
        };
      });
    }
  }]);

  return Teucer;
}();
