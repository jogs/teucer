const XMLHttpRequest = window.XMLHttpRequest

module.exports = class Teucer {
  constructor (url) {
    this.url = url
    return this
  }
  get (payload = {}, headers = {}) {
    return this.method('GET', this.url, payload, headers)
  }
  post (payload = {}, headers = {}) {
    return this.method('POST', this.url, payload, headers)
  }
  put (payload = {}, headers = {}) {
    return this.method('PUT', this.url, payload, headers)
  }
  delete (payload = {}, headers = {}) {
    return this.method('DELETE', this.url, payload, headers)
  }
  request () {
    let returnEvent = (evt) => evt
    let progressCalc = (evt) => {
      (evt.lengthComputable) ? evt.loaded / evt.total : 0
    }
    let ajax = new XMLHttpRequest()
    ajax.setHeaders = (headers) => {
      for (let header in headers) {
        if (headers.hasOwnProperty(header)) {
          ajax.setRequestHeader(header, headers[header])
        }
      }
    }
    ajax.onprogress = progressCalc
    ajax.onload = returnEvent
    ajax.onerror = returnEvent
    ajax.onabort = returnEvent
    ajax.onloadend = returnEvent
    ajax.onloadstart = returnEvent
    ajax.upload.onprogress = progressCalc
    ajax.upload.onload = returnEvent
    ajax.upload.onerror = returnEvent
    ajax.upload.onabort = returnEvent
    ajax.upload.onloadend = returnEvent
    ajax.upload.onloadstart = returnEvent
    ajax.onreadystatechange = () => {
      if (ajax.readyState === 4) return ajax.responseText
    }
    return ajax
  }
  method (method, url, payload, headers = {}) {
    let ajax = this.request()
    return new Promise((resolve, reject) => {
      let uri = url
      if (method === 'GET' || method === 'DELETE') {
        uri += '?'
        let argcount = 0
        for (let key in payload) {
          if (payload.hasOwnProperty(key)) {
            if (argcount++) {
              uri += '&'
            }
            uri += `${encodeURIComponent(key)}=${encodeURIComponent(payload[key])}`
          }
        }
        ajax.open(method, uri, true)
        ajax.setHeaders(headers)
        ajax.send()
      } else {
        ajax.open(method, uri, true)
        ajax.setHeaders(headers)
        if (payload.constructor === window.FormData.constructor) {
          ajax.send(payload)
        } else {
          ajax.setRequestHeader('Content-Type', 'application/json')
          ajax.send(JSON.stringify(payload))
        }
      }
      ajax.onload = function () {
        if (this.status >= 200 && this.status < 300) {
          resolve(JSON.parse(this.response))
        } else {
          if (/^\{|\[/.test(this.response)) {
            reject(JSON.parse(this.response))
          } else {
            reject(this.response)
          }
        }
      }
      ajax.onerror = function () {
        reject(this.statusText)
      }
    })
  }
}

